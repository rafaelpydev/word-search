import requests
from bs4 import BeautifulSoup


# def seach(word, *args):
#     tags = ('h1', 'h2', 'h3', 'h4', 'p', 'a', 'span')
#
#     for url in args:
#         request = requests.get(url)
#         soup = BeautifulSoup(request.text, 'html.parser')
#
#         li = []
#         for frase in soup.find_all(tags):
#             frase_list = str(frase.text).lower().split()
#             for palavra in frase_list:
#                 li.append(palavra)
        # print(url)
        # print(li)
        # print(li.count(word.lower()))



# print(seach('empresa', 'http://www.buildup.com.br/buildup/',
#             'http://www.tcibpo.com/'))


def seach(word, url):
    tags = ('h1', 'h2', 'h3', 'h4', 'p', 'a', 'span')

    request = requests.get(url)
    soup = BeautifulSoup(request.text, 'html.parser')

    li = []
    for frase in soup.find_all(tags):
        frase_list = str(frase.text).lower().split()
        for palavra in frase_list:
            li.append(palavra)
    # print(url)
    # print(li)
    # print(li.count(word.lower()))
    return li.count(word.lower())


print(seach('github', 'http://www.django-rest-framework.org/'))

from rest_framework import serializers
from search.models import Word, Url, View


class WordSerializer(serializers.ModelSerializer):

    class Meta:
        model = Word
        fields = ('word',)


class UrlSerializer(serializers.ModelSerializer):

    class Meta:
        model = Url
        fields = ('url', 'found_word')


class ViewSerializer(serializers.Serializer):
    word = WordSerializer()
    urls = UrlSerializer(many=True)

    def create_urls(self, urls, view):
        for url in urls:
            create_url = Url.objects.create(**url)
            view.urls.add(create_url)

    def create(self, validated_data):
        word = validated_data['word']
        del validated_data['word']

        urls = validated_data['urls']
        del validated_data['urls']

# TODO

        return view





from django.db import models
from .crawler import search


class Word(models.Model):
    word = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.word


class Url(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    url = models.URLField()

    def found_word(self):
        return search(str(self.word), self.url)

    def __str__(self):
        return self.url


class View(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    urls = models.ManyToManyField(Url)

    def __str__(self):
        return "View {}".format(self.word)
from django.contrib import admin
from .models import Url, Word, View

admin.site.register(Url)
admin.site.register(Word)
admin.site.register(View)
